# Ansible Role: Inframap

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-inframap/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-inframap/-/commits/main)

This role installs [Inframap](https://github.com/cycloidio/inframap/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    inframap_version: latest # v0.6.6 if you want a specific version
    inframap_arch: amd64 # amd64 or 386
    setup_dir: /tmp
    inframap_bin_path: /usr/local/bin/inframap
    inframap_repo_path: https://github.com/cycloidio/inframap/releases/download

This role can install the latest or a specific version. See [available Inframap releases](https://github.com/cycloidio/inframap/releases/) and change this variable accordingly.

    inframap_version: latest # v0.6.6 if you want a specific version

The path of the inframap repository.

    inframap_repo_path: https://github.com/cycloidio/inframap/releases/download

The location where the inframap binary will be installed.

    inframap_bin_path: /usr/local/bin/inframap

Inframap supports amd64 and 386 CPU architectures, just change for the main architecture of your CPU.

    inframap_arch: amd64 # amd64 or 386

Inframap needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install inframap. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: inframap

## License

MIT / BSD
